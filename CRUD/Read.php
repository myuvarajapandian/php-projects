<?php
include 'function.php';

// if (isset($_POST['read'])) {
// 	read();
// }
// include 'db.php';

// 	$query = "SELECT * FROM users";
// 	$output = mysqli_query($conn,$query);

// 	if(!$output){
// 		die('Query failed');
// 	}


?>

<!doctype html>
<html lang="en">

<head>
	<title>CURD</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="css/style.css">

</head>

<body>
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">READ</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-5">
					<div class="login-wrap p-4 p-md-5">
						<div class="icon d-flex align-items-center justify-content-center">
							<span class="fa fa-user-o"></span>
						</div>
						<form action="Read.php" class="login-form" method="post">

							<!-- for print all array -->
							<div class="form-group d-flex">
								<select name="id">
									<?php
									showAllData();
									?>
								</select>
							</div>
							<div class="form-group d-flex">
								<?php
								if (isset($_POST['read'])) {
									read();
								}
								?>
							</div>
							<!-- //  while ($row = mysqli_fetch_assoc($output)) { -->

							<!-- <?php
									//  	print_r($row);
									//  }
									?> -->

							<!-- for print particular column -->
							<!-- <?php

									// while ($row = mysqli_fetch_assoc($output)) {

									// 	
									?>
						// 	<br>
						// 	<?php
								// 	print_r($row['username']);
								// }
								?> -->


							<!-- print with pre tag for separate lines -->
							<!-- <?php

									//  while ($row = mysqli_fetch_assoc($output)) {
									// 
									?>
					 	// <pre>
					 	// <?php
							//  	print_r($row);
							// 
							?>	
						// </pre>
						// <?php
							//  }
							?> -->
							<div class="form-group">
								<button type="submit" class="btn btn-primary rounded submit p-3 px-5" name="read">Read</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</section>


</body>

</html>