<?php
include './db.php';

function Create($username, $email, $phone, $password)
{
    global $conn;

    // Hash the user's password
    $hashed_password = password_hash($password, PASSWORD_DEFAULT);

    $query = "INSERT INTO user(username, email, phone, password) VALUES ('$username','$email','$phone','$hashed_password')";
    $output = mysqli_query($conn, $query);

    if (!$output) {
        die('Query failed' . mysqli_error($conn));
    }
}

function login()
{
    global $conn;
    session_start();
    if (isset($_POST['submit'])) {
        $email = mysqli_real_escape_string($conn, $_POST['email']);
        $password = $_POST['password'];

        // Query the database to retrieve the hashed password
        $sql = "SELECT * FROM user WHERE email = '$email'";
        $result = mysqli_query($conn, $sql);

        if ($result) {
            if (mysqli_num_rows($result) == 1) {
                $row = mysqli_fetch_assoc($result);
                $db_pass = $row['password'];
                $db_user = $row['email']; // Assuming 'password' is the column name in the database

                // Check if the entered password matches the hashed password
                if (password_verify($password, $db_pass)) {
                    // Passwords match, user is authenticated
                    // echo "Login successful. Welcome, " . $row['username'];
                    header('location: index.php');
                    $_SESSION['role'] = 'user';
                    $_SESSION['email'] = $db_user;
                } else {
                    // Passwords do not match, deny access
                    echo "Incorrect password.";
                }
            } else {
                // User not found, deny access
                echo "User not found.";
            }
        } else {
            // Error executing the query
            echo "Error: " . mysqli_error($conn);
        }
    }
}

function reset_pass($password, $email) {

    global $conn;
    // Hash the user's password
    $hashed_password = password_hash($password, PASSWORD_DEFAULT);

    $query = "UPDATE user SET password = '$hashed_password' WHERE email = '$email'";
    $output = mysqli_query($conn, $query);

    if (!$output) {
        die('Query failed' . mysqli_error($conn));
    }
}
