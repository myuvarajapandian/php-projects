<?php
session_start();
require 'login_fun.php';

// $hostname = 'smtp.gmail.com'; // Replace with your SMTP server hostname
// $ip = gethostbyname($hostname);

// if ($ip === $hostname) {
//     echo "DNS resolution failed. Check your DNS configuration.";
// } else {
//     echo "Resolved IP address: " . $ip;
// }

// Check if passwords match
if (isset($_POST['submit'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirm_password = $_POST['cpassword'];

    if (!empty($password)) {
        if ($password === $confirm_password) {
            reset_pass($password, $email);
            echo "password Changed";
            header('location: login.php');
        } else {
            echo "Passwords do not match.";
        }
    } else {
        echo "Enter Confirm password.";
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <title>Reset Password</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="css/style.css">

</head>

<body class="img js-fullheight" style="background-image: url(images/bg.jpg);">
    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 text-center mb-5">
                    <h2 class="heading-section">Do you want to reset?</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-4">
                    <div class="login-wrap p-0">
                        <h3 class="mb-4 text-center">Enter Password</h3>
                        <form action="reset.php" class="signin-form" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Email" name="email" required>
                            </div>
                            <div class="form-group">
                                <input id="password-field" type="password" class="form-control" placeholder="Password" name="password" required>
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>
                            <div class="form-group">
                                <input id="password-field" type="password" class="form-control" placeholder="Confirm Password" name="cpassword" required>
                                <!-- <span toggle="#password-field" class="fa fa-fw fa-eye field-icon ctoggle-password"></span> -->
                            </div>
                            <div class="form-group">
                                <button type="submit" class="form-control btn btn-primary submit px-3" name="submit">Change Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>

</body>
<?php
if (isset($_SESSION['role'])) {
	if ($_SESSION['role'] == 'user') {
		header('location: index.php');
	}
}
?>