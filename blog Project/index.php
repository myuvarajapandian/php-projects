<?php

include './includes/db.php';
include './includes/head.php';

?>

<body>
    <!-- Navigation-->
    <?php include './includes/nav.php'; ?>
    <!-- Page Header-->
    <header class="masthead" style="background-image: url('assets/img/home-bg.jpg')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="site-heading">
                        <br><br>
                        <h1>Clean Blog</h1><br>
                        <br><br>
                        <span class="subheading">
                            <h2>A Blog Theme by Start Bootstrap</h2>
                        </span><br><br><br>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main Content-->
    <div class="container px-4 px-lg-5"><br>
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <?php
                $post_query = "SELECT * FROM posts";
                $res = mysqli_query($conn, $post_query);
                while ($row = mysqli_fetch_assoc($res)) {
                    $post_title = $row['post_title'];
                    $sub_title = $row['post_sub_title'];
                    $date = $row['post_date'];
                    $post_author = $row['post_author'];
                ?>
                <!-- Post preview-->
                <div class="post-preview">
                    <a href="post.php">
                        <h2 class="post-title"> <?php echo $post_title; ?></h2>
                        <h3 class="post-subtitle"> <?php echo $sub_title; ?> </h3>
                    </a>
                    <p class="post-meta">
                        Posted by
                        <a href="#!"> <?php echo $post_author; ?></a>
                        <?php echo $date; ?>
                    </p>
                </div>
                <!-- Divider-->
                <hr class="my-4" />
                <?php
                }
                ?>
                <!-- Pager-->
                <div class="d-flex justify-content-end mb-4"><a class="btn btn-primary text-uppercase" href="#!">Older
                        Posts →</a></div>
            </div>
        </div>
    </div>
    <!-- Footer-->
    <?php include './includes/footer.php'; ?>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
</body>

</html>