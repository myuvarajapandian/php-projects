<?php
include 'db.php';
function showAllData()
{
    global $conn;
    $query = "SELECT * FROM users";
    $result = mysqli_query($conn, $query);

    if (!$result) {
        die('Query failed' . mysqli_error($conn));
    }

    while ($row = mysqli_fetch_assoc($result)) {
        $id = $row['id'];
        echo "<option value='$id'>$id</option>";
    }
}

function create()
{

    global $conn;
    if (isset($_POST['Create'])) {
    //    $username = $_POST['username'];
    //    $password = $_POST['password'];
       
        // sql injection preventing method
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $password = mysqli_real_escape_string($conn, $_POST['password']);
        $password = crypt($password,'$2a$07$usesomesillystringyuvaraj$');
       

        $query = "INSERT INTO users(username,password) VALUES ('$username','$password');";
        $output = mysqli_query($conn, $query);

        if (!$output) {
            die('Query failed' . mysqli_error($conn));
        }
    }
}

function read()
{
    global $conn;
    $id = $_POST['id'];
    $query = "SELECT * FROM users WHERE id  = '$id'";
    $output = mysqli_query($conn, $query);

    if (!$output) {
        die('Query failed' . mysqli_error($conn));
    }
    while ($row = mysqli_fetch_assoc($output)) {

?>
        <br>
<?php
        print_r($row);
    }
    return $output;
}

function update()
{
    global $conn;
    $username = $_POST['username'];
    $password = $_POST['password'];
    $id = $_POST['id'];

    $query = "UPDATE users SET username = '$username',password ='$password' WHERE id = '$id'";
    $result = mysqli_query($conn, $query);

    if (!$result) {
        die('Query failed');
    }
}

function delete()
{
    global $conn;
    $username = $_POST['username'];
    $query = "DELETE FROM users WHERE username = '$username'";
    $result = mysqli_query($conn, $query);
}
