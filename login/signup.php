<?php

include './db.php';
include './login_fun.php';

session_start();

if (isset($_POST['submit'])) {
    $username = mysqli_real_escape_string($conn, $_POST['name']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $phone = mysqli_real_escape_string($conn, $_POST['phone']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);
    $confirm_password = mysqli_real_escape_string($conn, $_POST['cpassword']);

    // Check if passwords match
    if (!empty($password)) {
        if ($password === $confirm_password) {
            Create($username, $email, $phone, $password);
            header('location: login.php');
        } else {
            echo "Passwords do not match.";
        }
    } else {
        echo "Enter Confirm password.";
    }
}

?>

<!doctype html>
<html lang="en">

<head>
	<title>Sign Up</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="css/style.css">

</head>

<body class="img js-fullheight" style="background-image: url(images/bg.jpg);">
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Sign Up</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-4">
					<div class="login-wrap p-0">
						<form action="signup.php" class="signin-form" method="post">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Name" name="name" required>
							</div>
							<div class="form-group">
								<input type="email" class="form-control" placeholder="Email" name="email" required>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Phone" name="phone" required>
							</div>
							<div class="form-group">
								<input id="password-field" type="password" class="form-control" placeholder="Enter Password" name="password">
								<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
							</div>
							<div class="form-group">
								<input id="password-field" type="password" class="form-control" placeholder="Confirm Password" name="cpassword" required>
								<!-- <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span> -->
							</div>
							<div class="form-group">
								<button type="submit" class="form-control btn btn-primary submit px-3" name="submit">Sign Up</button>
							</div>
							<div class="form-group d-md-flex">
								<div class="w-50">
									<label class="checkbox-wrap checkbox-primary">Remember Me
										<input type="checkbox" checked>
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="w-50 text-md-right">
									<a href="login.php" style="color: #fff">Have an account?</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<script src="js/jquery.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>

</body>

</html>

<?php
if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 'user') {
        header('location: index.php');
    }
}
?>