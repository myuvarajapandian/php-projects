<?php
include '../includes/db.php';
include './includes/head.php';
?>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include './includes/sidebar.php'; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include './includes/topbar.php';  ?>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800">Category</h1>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-6 col-lg-6">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Add categories</h6>
                                </div>
                                <div class="card-body">
                                    <?php if (isset($_POST['Create'])) {
                                        $cat_title = $_POST['cat_title'];

                                        $insert = "INSERT INTO category(cat_title) VALUES ('$cat_title');";
                                        $output = mysqli_query($conn, $insert);
                                    } 
                                    
                                    ?>
                                    <form action="category.php" method="post">
                                        <input type="text" class="form-control" name="cat_title"><br>
                                        <input type="submit" class="btn btn-outline-primary" value="Create" name="Create">
                                    </form>
                                </div>
                            </div>
                        </div>


                        <div class="col-xl-6 col-lg-6">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">All categories</h6>
                                    <div class="dropdown no-arrow">
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Categories</th>
                                                <th>Remove</th>
                                                <th>Edit</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (isset($_POST['Delete'])) {
                                                // $cat_id = $_POST[''];
                                                // $query = "DELETE FROM users WHERE username = '$username'";
                                                $delete = "DELETE * FROM category WHERE cat_id = ''";
                                                $result = mysqli_query($conn, $delete);
                                            }
                                            $read = "SELECT * FROM category WHERE cat_id";
                                            $output = mysqli_query($conn, $read);
                                            while ($row = mysqli_fetch_assoc($output)) {
                                                $cat_id = $row['cat_id'];
                                                $cat_title = $row['cat_title'];
                                            ?>
                                                <tr>
                                                    <td><?php echo $cat_id; ?></td>
                                                    <td><?php echo $cat_title; ?></td>
                                                    <td>
                                                        <form action="category.php" method="post">
                                                            <input type="submit" class="btn btn-outline-primary" value="Delete" name="Delete">
                                                        </form>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <!-- Edit category -->
                        <!-- <div class="col-xl-6 col-lg-6">
                            <div class="card shadow mb-4">

                                <-- Card Header - Dropdown -->
                        <!--<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Edit categories</h6>
                                </div>
                                <div class="card-body">
                                    <form action="" method="post">
                                        <input type="text" class="form-control" name="cat_title" value=""><br>
                                        <input type="submit" class="btn btn-primary" value="update" name="update">
                                    </form>
                                </div>
                            </div>
                        </div> -->


                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <?php include './includes/footer.php'; ?>